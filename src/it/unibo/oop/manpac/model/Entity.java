package it.unibo.oop.manpac.model;

/**
 * Represents the entities in the game.
 *
 */
public enum Entity {

    /**
     * Represents a wall.
     *
     */
    WALL,
    /**
     * Represents a pill.
     *
     */
    PILL,
    /**
     * Represents the main character Pacman.
     *
     */
    PACMAN,
    /**
     * Represents a power pill.
     *
     */
    POWERPILL,
    /**
     * Represents an enemy, a phantom.
     *
     */
    PORTAL,
    /**
     * Represents a tunnel.
     *
     */
    BLINKY,
    /**
     * Represents the pink phantom.
     *
     */
    PINKY,
    /**
     * Represents the light blue phantom.
     *
     */
    INKY,
    /**
     * Represents the orange phantom.
     *
     */
    CLYDE,
    /**
     * Represents a generic spawnPoint.
     */
    SPAWN

}
